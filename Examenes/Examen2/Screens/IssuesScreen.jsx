import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, TouchableOpacity, ScrollView } from "react-native";


import { fetchALL } from "../api";

export default function IssuesScreen() {
    const [todos, setTodos] = useState([]);
    const [viewMode, setViewMode] = useState('ids'); 

    useEffect(() => {
        const fetchData = async () => {
            const data = await fetchALL();
            setTodos(data);
        };
        
        fetchData();
    }, []);

    return (
        <View style={styles.container}>
            <Text style={styles.header}>Lista de Tareas</Text>
            <View style={styles.buttonContainer}>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => setViewMode('ids')}
                >
                    <Text style={styles.buttonText}>Solo IDs</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => setViewMode('details')}
                >
                    <Text style={styles.buttonText}>IDs y Títulos</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => setViewMode('userIds')}
                >
                    <Text style={styles.buttonText}>IDs y UserIDs</Text>
                </TouchableOpacity>
                </View>
            <ScrollView style={styles.scrollView}>
                {todos.map(todo => (
                    <View key={todo.id} style={styles.todoContainer}>
                        <Text style={styles.todoText}>ID: {todo.id}</Text>
                        {viewMode === 'details' && <Text style={styles.todoText}>Título: {todo.title}</Text>}
                        {viewMode === 'userIds' && <Text style={styles.todoText}>UserID: {todo.userId}</Text>}
                    </View>
                ))}
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop: 20,
    },
    header: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly', 
        width: '100%', 
        marginBottom: 20,
    },
    scrollView: {
        width: '100%',
    },
    todoContainer: {
        borderWidth: 1,
        borderColor: '#ddd',
        padding: 10,
        marginVertical: 5,
        borderRadius: 5,
        width: '90%',
        alignSelf: 'center', 
    },
    todoText: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    button: {
        backgroundColor: '#fff', 
        borderColor: '#000000', 
        borderWidth: 2, 
        borderRadius: 5, 
        padding: 10, 
        marginHorizontal: 5, 
    },
    buttonText: {
        color: '#000000', 
        fontWeight: 'bold', 
    },
  
});