import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";

export default function HomeScreen() {
    return (
        <View style={styles.container}>
            <Text style={styles.welcomeText}>Bienvenido a la NFL</Text>
            <Image
                source={{ uri: "https://cf-images.us-east-1.prod.boltdns.net/v1/static/854081161001/28841c18-9e46-4f9b-a64a-e0e3f939574f/3b37ed59-d7e7-4907-90f7-7edfce57d853/1280x720/match/image.jpg" }}
                style={styles.image}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff', 
        alignItems: 'center',
        justifyContent: 'center',
    },
    welcomeText: {
        fontSize: 24, 
        fontWeight: 'bold',
        color: '#000000', 
        marginBottom: 20, 
    },
    image: {
        width: 250, 
        height: 250, 
        borderRadius: 75, 
        backgroundColor: '#000000', 
    },
});







