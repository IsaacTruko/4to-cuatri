import React from "react";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

//SCREENS
import HomeScreen from './Screens/HomeScreen';
import IssuesScreen from './Screens/IssuesScreen';
import CompleteIssueScreen from './Screens/CompleteIssueScreen';
import UncompleteIssueScreen from './Screens/UncompleteIssueScreen';

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

 function Mytabs() {

    return (
            <Tab.Navigator>
                <Tab.Screen name="NFL" component={HomeScreen} />
                <Tab.Screen name="Issues" component={IssuesScreen} />
                <Tab.Screen name="Complete" component={CompleteIssueScreen} />
                <Tab.Screen name="Uncomplete" component={UncompleteIssueScreen} />
            </Tab.Navigator>
    );
    
}

function MyStack() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Home" component={Mytabs} />
        </Stack.Navigator>
    );
}

export default function Navigation(){

    return (
        <NavigationContainer>
            <Mytabs />
        </NavigationContainer>
    );
}