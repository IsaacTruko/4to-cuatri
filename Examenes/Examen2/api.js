const API_URL = 'http://jsonplaceholder.typicode.com/todos';

export const fetchALL = async () => {
  try {
    const response = await fetch(API_URL);
    if (!response.ok) {
      throw new Error('Error al obtener los datos de la API');
    }
    return await response.json();
  } catch (error) {
    console.error('Hubo un problema con la petición Fetch', error);
    throw error;
  }
};