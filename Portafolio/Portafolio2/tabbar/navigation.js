import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from "@react-navigation/native";

// screens
import HomeScreen from "./screens/HomeScreen";
import ProfileScreen from "./screens/ProfileScreen";
import SettingsScreen from "./screens/SettingsScreen";
import StackScreen from "./screens/StackScreen";


const tab = createBottomTabNavigator();
const stack = createNativeStackNavigator();

function MyTabs() {
    return (
            <tab.Navigator>
                <tab.Screen name="Home" component={MyStack} />
                <tab.Screen name="Profile" component={ProfileScreen} />
                <tab.Screen name="Settings" component={SettingsScreen} />
            </tab.Navigator>
    );
}

function MyStack() {
    return (
        <stack.Navigator>
            <stack.Screen name="HomeScreen" component={HomeScreen} />
            <stack.Screen name="Stack" component={StackScreen} />
        </stack.Navigator>
    );
}

export default function Navigation() {
    return (     
        <NavigationContainer>   
        <MyTabs />
        </NavigationContainer>
    );
}

