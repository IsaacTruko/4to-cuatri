// Ejercicio 1: Variables y operaciones básicas
// console.log("Hola mundo")

// var s = "Foo bar"

// let x = 90
// var y = 89

// console.log(s)
// console.log(x + y)

// s = true

// console.log(s)

// Ejercicio 2: Arrays y objetos
// array = [1,2,3,4,5, "Foo", "Bar", true, false, 2.34, 4.23]

// console.log(array[5])

// var obj = {
//     first_name: "Foo",
//     last_name: "Bar", 
//     age: 23,  
//     city: "TJ", 
//     status: true
// }

// console.log(obj)

// console.log(obj["first_name"])
// console.log(obj.last_name)

// for (let i = 0; i < array.length; i++) {
//     console.log(array[i])   
// }

// Ejercicio 3: Bucles
// for (let i = 0; i < 100; i+=5) {
//     console.log(i)
// }

// for (let i of array) {
//     console.log(i)
// }

// for (let key of Object.keys(obj)) {
//     console.log(key)
// }

// var i = 0
// while(i < 145) {
//     console.log(i);
//     i+=5
// }

// Ejercicio 4: Condicionales
// x = 9
// y = 10

// if (x > y) {
//     console.log("Hola");
// } else {
//     console.log("No hola");
// }

// var animal = 'kitty'

// var hello = (animal === 'kitty') ? 'it is a pretty kitty' : 'is not a pretty kitty' 

// console.log(hello);

// var opc = 2

// switch (opc) {
//     case 1:
//         console.log("Case 1");
//         break;

//     case 2:
//         console.log("Case 2");
//         break;
    
//     case 5:
//         console.log("Case 5");
//         break;
//     default:
//         break;
// }

// Ejercicio 5: Funciones
// function foo(msg = "world!") {
//     console.log(msg);
// }

// foo()

// Ejercicio 6: Cierres (closures)
// function prism(l) {
//     return function(w) {
//         return function(h) {
//             return l*w*h
//         }
//     }
// }

// console.log("el volumen del prisma es " + prism(20)(20)(15));

// Ejercicio 7: Asincronía y Fetch API
// var url = 'http://api.stackexchange.com/2.2/questions?site=stackoverflow&tagged=javascript';

// var responseData = fetch(url).then(response => response.json());
// responseData.then(({items, has_more, quota_max, quota_remaining}) => {
//     for (const {title, score, owner, link, answer_count} of items) {
//         console.log("Q: "+ title + "owner: " + owner.user_id);
//     }
// });

// Ejercicio 8: Manipulación del DOM
// const questionList = document.createElement('ul');
// document.body.appendChild(questionList);

// Ejercicio 9: Fetch API con POST
// var url = 'https://jsonplaceholder.typicode.com/posts';

// fetch(url, {
//     method: "POST",
//     headers: {
//         "Content-Type": "application/json"
//     },
//     body: JSON.stringify({
//         userId: 101, 
//         title: "Nuevo Post",
//         body: "Este es un nuevo POST."
//     })
// })
//     .then(response => response.json())
//     .then(newPost => console.log('New Post:', newPost))

// Ejercicio 10: Manejo de errores con Fetch API
// var url = 'https://jsonplaceholder.typicode.com/users';

// var responseData = fetch(url)
// .then(response => response.json())
// responseData.then(users => {
//     for (const { id, name, username, email } of users) {
//         console.log("username: " + username);
//         }
//     })
// .catch(error => console.error('Error fetching data:', error));

// Ejercicio 11: Otros ejemplos con Fetch API
// var url = 'https://jsonplaceholder.typicode.com/posts';

// fetch(url)
//     .then(response => response.json())
//     .then(posts => {
//         for (const { userId, id, title, body } of posts) {
//             console.log(`Post ID: ${id}, User ID: ${userId}, Title: ${title}, Body: ${body}`);
//         }
//     })

// var url = 'https://jsonplaceholder.typicode.com/posts';

// fetch(url)
// .then(response => response.json())
// .then(posts => {
//     for (const { id, title, body } of posts) {
//         console.log(`Post ID: ${id}, Title: ${title}, Body: ${body}`);
//     }
// })






