export { default as LoginScreen } from './LoginScreen'
export { default as ResetPasswordScreen } from './ResetPasswordScreen'
//Vistas Empleado
export { default as HerramientasScreen } from './Empleados/HerramientasScreen'
export { default as SolicitudesScreen } from './Empleados/SolicitudesScreen'
export { default as MantenimientosScreen } from './Empleados/MantenimientosScreen'
export { default as TareasScreen} from './Empleados/TareasScreen'
export { default as AddMantenimientoScreen} from './Empleados/AddMantenimientoScreen'
//Vistas Supervisor
