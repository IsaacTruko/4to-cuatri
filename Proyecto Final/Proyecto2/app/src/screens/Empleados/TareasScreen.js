import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, ScrollView } from 'react-native';
import Background from '../../components/Background';
import BackButton from '../../components/BackButton';
import Header from '../../components/Header';

const TareasScreen= ({ onBack, onGenerateReport, navigation }) => {
  // Datos simulados de la tarea, que normalmente obtendrías de una API o de tu estado global
  const taskDetails = {
    name: 'Inspección de Equipos',
    place: 'Almacén 3',
    machineNumber: 'MCH-009',
    date: '2024-03-06',
    problemDescription: 'Revisión periódica del equipo de acuerdo al protocolo.',
    priority: 'Alta',
    status: 'En progreso',
    supervisorName: 'Juan Pérez',
  };

  return (
    <Background>
    <Header>Mis Tareas</Header>
    <ScrollView style={styles.container}>
      <TouchableOpacity onPress={onBack} style={styles.backButton}>
        <Text>Volver</Text>
      </TouchableOpacity>
      <View style={styles.details}>
        <Text>Nombre de la tarea: {taskDetails.name}</Text>
        <Text>Lugar: {taskDetails.place}</Text>
        <Text>Número de la máquina: {taskDetails.machineNumber}</Text>
        <Text>Fecha: {taskDetails.date}</Text>
        <Text>Descripción del problema: {taskDetails.problemDescription}</Text>
        <Text>Prioridad: {taskDetails.priority}</Text>
        <Text>Estado: {taskDetails.status}</Text>
        <Text>Nombre del supervisor: {taskDetails.supervisorName}</Text>
      </View>
      <TouchableOpacity onPress={onGenerateReport} style={styles.reportButton}>
        <Text>Generar reporte</Text>
      </TouchableOpacity>
    </ScrollView>
    </Background>

  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  backButton: {
    marginBottom: 10,
    paddingHorizontal: 10,
    paddingVertical: 5,
    alignSelf: 'flex-start',
  },
  details: {
    marginVertical: 20,
  },
  reportButton: {
    backgroundColor: 'green',
    padding: 15,
    borderRadius: 5,
    alignItems: 'center',
    marginTop: 20,
  },
});

export default TareasScreen;
