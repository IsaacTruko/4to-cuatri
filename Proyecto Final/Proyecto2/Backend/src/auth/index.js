const jwt = require('jsonwebtoken');
config = require('../config');

const secret = config.jwt.secret;

function giveToken(data) {
    return jwt.sign(data, secret, { expiresIn: '1h' });
}

function verifyToken(token) {
    return jwt.verify(token, secret);
}

const checkToken = {
    confirmToken: function(req, res, next) {
        const decoded = decodeHeaders(req.params.header);
    
        if(decoded.id !== id) {
            throw new Error('No puedes hacer esto');
        } 
    }
}

function getToken(Authorization) {
    if(!Authorization) {
        throw new Error('No autorizado');
    }
    
    if(Authorization.indexOf('Bearer') === -1) {
        throw new Error('Formato invalido');
    }

    let token = Authorization.replace('Bearer ', '');
    return token;
}

function decodeHeaders(req) {
    const Authorization = req.headers.autorization || '';
    const token = getToken(Authorization);
    const decoded = verifyToken(token);

    req.user = decoded;

    return decoded;
}

module.exports = {
    giveToken
}
