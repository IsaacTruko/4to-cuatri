const mysql = require('mysql');
const config = require('../config');



const dbconfig = {
    host: config.mysql.host,
    user: config.mysql.user,
    password: config.mysql.password,
    database: config.mysql.database
};


let conexion;

function conmysql(){
    conexion = mysql.createConnection(dbconfig);
    return new Promise((resolve, reject) => {
        if (conexion) {
            console.log('Conectado a la base de datos');
            resolve();
        } else {
            console.log('No se pudo conectar a la base de datos');
            reject();

        }
    });
}

conmysql();

function all(table) { 
    return new Promise((resolve, reject) => {
        if (conexion) {
            const sql = `SELECT * FROM ${table} WHERE activo = 1`;
            const query = conexion.query(sql, (err, result) => {
                if (err) reject(err);
                resolve(result);
            });
        }        
    })
}

function one(table, id) { 
    return new Promise((resolve, reject) => {
        conexion.query(`SELECT * FROM ${table} WHERE id = ${id}`, (err, result) => {
            return err ? reject(err) : resolve(result)
        })
    })
}

function add(table, data) { 
    return new Promise((resolve, reject) => {
        conexion.query(`INSERT INTO ${table} SET ? ON DUPLICATE KEY UPDATE ?`, [data, data], (err, result) => {
            return err ? reject(err) : resolve(result)
        })
    })
}


function eraseone(table, data) { 
    return new Promise((resolve, reject) => {
        conexion.query(`DELETE FROM ${table} WHERE id = ?`, [data.id], (err, result) => {
            return err ? reject(err) : resolve(result)
        })
    })
}

function disable(table, data) { 
    return new Promise((resolve, reject) => {
        conexion.query(`UPDATE ${table} SET activo = 0 WHERE id = ?`, [data.id], (err, result) => {
            return err ? reject(err) : resolve(result)
        })
    })
}

function query(TABLE, consult) { 
    return new Promise((resolve, reject) => {
        conexion.query(`SELECT * FROM ${TABLE} WHERE ?`, consult, (err, result) => {
            return err ? reject(err) : resolve(result[0])
        })     
    })

}

function changePassword(TABLE, data) { 
    return new Promise((resolve, reject) => {
        conexion.query(`UPDATE ${TABLE} SET password = ? WHERE id = ?`, [data.password, data.id], (err, result) => {
            return err ? reject(err) : resolve(result)
        })
    })
}

function selectSupervisor(id) {
    return new Promise((resolve, reject) => {
        const sql = `
        SELECT 
        Sup.usuario_id,
        User.nombrePila,
        Sup.departamento_id,
        Depto.nombre
        FROM Supervisor AS Sup
        INNER JOIN Departamento AS Depto ON Depto.id = Sup.departamento_id
        INNER JOIN Usuario AS User ON User.id = Sup.usuario_id
        WHERE User.activo = 1;
        `;
        conexion.query(sql, [id], (err, results) => {
            if (err) {
                reject(err);
            } else {
                resolve(results);
            }
        });
    });
}

function selectAllSupervisor() {
    return new Promise((resolve, reject) => {
      const sql = `
        SELECT
          Sup.usuario_id,
          User.nombrePila,
          Sup.departamento_id,
          Depto.nombre
        FROM Supervisor AS Sup
        INNER JOIN Departamento AS Depto ON Depto.id = Sup.departamento_id
        INNER JOIN Usuario AS User ON User.id = Sup.usuario_id
        WHERE User.activo = 1;
      `;
  
      conexion.query(sql, (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }

function selectManteMaqui(id){
    return new Promise((resolve, reject) => {
        const sql = `
        SELECT
        MM.maquina_id,
        Maqui.nombre,
        MM.mantenimiento_id,
        Mante.descripcion
        FROM ManteMaqui AS MM
        INNER JOIN Maquina as Maqui ON Maqui.id = MM.maquina_id
        INNER JOIN Mantenimiento as Mante ON Mante.id = MM.mantenimiento_id
        WHERE Maqui.activo = 1
        `;
        conexion.query(sql, [id], (err, results) => {
            if (err) {
                reject(err);
            } else {
                resolve(results);
            }
        });
    })
}

function selectAllManteMaqui(){
  return new Promise((resolve, reject) => {
      const sql = `
      SELECT
      MM.maquina_id,
      Maqui.nombre,
      MM.mantenimiento_id,
      Mante.descripcion
      FROM ManteMaqui AS MM
      INNER JOIN Maquina as Maqui ON Maqui.id = MM.maquina_id
      INNER JOIN Mantenimiento as Mante ON Mante.id = MM.mantenimiento_id
      WHERE Maqui.activo = 1
      `;
      conexion.query(sql, (err, results) => {
          if (err) {
              reject(err);
          } else {
              resolve(results);
          }
      });
  })
}


function selectReporte(id) {
    return new Promise((resolve, reject) => {
      const sql = `
        SELECT
          Rep.id,
          Rep.descripcion,
          Rep.fechaInicio,
          Rep.fechaFinal,
          Rep.usuario_id,
          User.nombrePila
        FROM Reporte AS Rep
        INNER JOIN Usuario as User ON User.id = Rep.usuario_id
      `;
  
      conexion.query(sql, [id], (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }

  function selectAllReporte() {
    return new Promise((resolve, reject) => {
      const sql = `
        SELECT
          Rep.id,
          Rep.descripcion,
          Rep.fechaInicio,
          Rep.fechaFinal,
          Rep.usuario_id,
          User.nombrePila
        FROM Reporte AS Rep
        INNER JOIN Usuario as User ON User.id = Rep.usuario_id
      `;
  
      conexion.query(sql, (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }
  
  function selectTarea(id) {
    return new Promise((resolve, reject) => {
      const sql = `
        SELECT
          t.id,
          t.prioridad,
          s.descripcion AS solicitud_descripcion,
          p.descripcion AS problema_descripcion,
          m.descripcion AS mantenimiento_descripcion,
          t.activo
        FROM
          Tarea t
          INNER JOIN Solicitud s ON t.solicitud_id = s.id
          INNER JOIN Problema p ON t.problema_id = p.id
          INNER JOIN Mantenimiento m ON t.mantenimiento_id = m.id
      `;
  
      conexion.query(sql, [id], (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }

  function selectAllTarea() {
    return new Promise((resolve, reject) => {
      const sql = `
        SELECT
          t.id,
          t.prioridad,
          s.descripcion AS solicitud_descripcion,
          p.descripcion AS problema_descripcion,
          m.descripcion AS mantenimiento_descripcion,
          t.activo
        FROM
          Tarea t
          INNER JOIN Solicitud s ON t.solicitud_id = s.id
          INNER JOIN Problema p ON t.problema_id = p.id
          INNER JOIN Mantenimiento m ON t.mantenimiento_id = m.id
      `;
  
      conexion.query(sql, (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }
  
  function selectSolicitud(id) {
    return new Promise((resolve, reject) => {
      const sql = `
        SELECT
          s.id,
          s.hora,
          s.descripcion,
          s.fecha,
          us.nombrePila AS usuario_solicitante,
          us.apPat AS ap_solicitante,
          us.apMat AS am_solicitante,
          ur.nombrePila AS usuario_resolvio,
          ur.apPat AS ap_resolvio,
          ur.apMat AS am_resolvio,
          e.nombre AS estado,
          s.activo
        FROM
          Solicitud s
          INNER JOIN Usuario us ON s.usuario_id_solicita = us.id
          INNER JOIN Usuario ur ON s.usuario_id_resuelve = ur.id
          INNER JOIN Estado e ON s.estado_id = e.id
      `;
  
      conexion.query(sql, [id], (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }

  function selectAllSolicitud() {
    return new Promise((resolve, reject) => {
      const sql = `
        SELECT
          s.id,
          s.hora,
          s.descripcion,
          s.fecha,
          us.nombrePila AS usuario_solicitante,
          us.apPat AS ap_solicitante,
          us.apMat AS am_solicitante,
          ur.nombrePila AS usuario_resolvio,
          ur.apPat AS ap_resolvio,
          ur.apMat AS am_resolvio,
          e.nombre AS estado,
          s.activo
        FROM
          Solicitud s
          INNER JOIN Usuario us ON s.usuario_id_solicita = us.id
          INNER JOIN Usuario ur ON s.usuario_id_resuelve = ur.id
          INNER JOIN Estado e ON s.estado_id = e.id
      `;
  
      conexion.query(sql, (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }
  
  function selectMisTareas(id) {
    return new Promise((resolve, reject) => {
      const sql = `
        SELECT
          mt.usuario_id,
          u.nombrePila AS usuario,
          u.apPat,
          u.apMat,
          t.id AS tarea_id,
          t.prioridad,
          s.descripcion AS solicitud_descripcion
        FROM
          MisTareas mt
          INNER JOIN Usuario u ON mt.usuario_id = u.id
          INNER JOIN Tarea t ON mt.tarea_id = t.id
          INNER JOIN Solicitud s ON t.solicitud_id = s.id
      `;
  
      conexion.query(sql, [id], (err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }

  function selectAllMisTareas() {
    return new Promise((resolve, reject) => {
      const sql = `
        SELECT
          mt.usuario_id,
          u.nombrePila AS usuario,
          u.apPat,
          u.apMat,
          t.id AS tarea_id,
          t.prioridad,
          s.descripcion AS solicitud_descripcion
        FROM
          MisTareas mt
          INNER JOIN Usuario u ON mt.usuario_id = u.id
          INNER JOIN Tarea t ON mt.tarea_id = t.id
          INNER JOIN Solicitud s ON t.solicitud_id = s.id
      `;
  
      conexion.query(sql,(err, results) => {
        if (err) {
          reject(err);
        } else {
          resolve(results);
        }
      });
    });
  }






module.exports = {
    all,
    one,
    add,
    disable,
    eraseone,
    query,
    changePassword,
    selectSupervisor,
    selectAllSupervisor,
    selectManteMaqui,
    selectAllManteMaqui,
    selectReporte,
    selectAllReporte,
    selectTarea,
    selectAllTarea,
    selectSolicitud,
    selectAllSolicitud,
    selectMisTareas,
    selectAllMisTareas
}