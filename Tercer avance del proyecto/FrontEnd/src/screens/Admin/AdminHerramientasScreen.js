import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default class AdminHerramientasScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.header}>Pantalla para gestionar herramientas</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    justifyContent: 'center', 
    alignItems: 'center', 
    backgroundColor: '#f5f5f5',
  },
  header: {
    fontSize: 22,
    fontWeight: 'bold',
    marginBottom: 20,
  },
});