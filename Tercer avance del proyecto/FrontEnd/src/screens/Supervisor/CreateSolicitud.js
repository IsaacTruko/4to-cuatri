import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
  Text,
  TextInput,
  Alert,
  Platform,
} from "react-native";
import { departmentValidator } from "../../helpers/departmentValidator";
import { priorityValidator } from "../../helpers/priorityValidator";
import { maintenanceValidator } from "../../helpers/maintenanceValidator";
import { machineNumberValidator } from "../../helpers/machineNumberValidator";
import { descriptionValidator } from "../../helpers/descriptionValidator";

import Background from "../../components/Background";
import Header from "../../components/Header";
import moment from "moment";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { SERVER_IP } from "../../config";
import RNPickerSelect from "react-native-picker-select";
import { sendMessageViaWhatsApp } from "./whatsapp";

const CreateSolicitud = ({}) => {
  const [place, setPlace] = useState("");
  const [machineNumber, setMachineNumber] = useState("");
  const [date, setDate] = useState("");
  const [description, setDescription] = useState("");
  const [priority, setPriority] = useState("");
  const [time, setTime] = useState("");
  const [assignee, setAssignee] = useState("");
  const [supervisorId, setSupervisorId] = useState(null);
  const [employees, setEmployees] = useState([]);
  const [departamentos, setDepartamentos] = useState("");
  const [mantenimiento, setMantenimiento] = useState("");
  const [taskName, setTaskName] = useState("Tarea");

  const priorityOptions = [
    { label: "Baja", value: "Baja" },
    { label: "Media", value: "Media" },
    { label: "Alta", value: "Alta" },
  ];

  const DepartmentOptions = [
    { label: "Produccion", value: "Produccion" },
    { label: "Almacen", value: "Almacen" },
    { label: "Seguridad", value: "Seguridad" },
    { label: "Intendencia", value: "Intendencia" },
  ];

  const MantOptions = [
    { label: "Urgente", value: "1" },
    { label: "Preventivo", value: "2" },
    { label: "Programado", value: "3" },
    { label: "Diario", value: "4" },
  ];

  const MachineOptions = [
    { label: "N/A", value: "14" },
    { label: "SMT-1", value: "1" },
    { label: "SMT-2", value: "2" },
    { label: "SMT-3", value: "3" },
    { label: "SMT-4", value: "4" },
    { label: "SMT-5", value: "5" },
    { label: "SMT-6", value: "6" },
    { label: "SMT-7", value: "7" },
    { label: "SMT-8", value: "8" },
    { label: "SMT-9", value: "9" },
    { label: "SMT-10", value: "10" },
    { label: "SMT-11", value: "11" },
    { label: "SMT-12", value: "12" },
    { label: "SMT-13", value: "13" },
  ];

  useEffect(() => {
    const fetchUserInfo = async () => {
      try {
        const userId = await AsyncStorage.getItem("userId");
        setSupervisorId(userId);
      } catch (error) {
        //console.error("Error al obtener el ID del usuario:", error);
      }
    };

    const fetchEmployees = async () => {
      try {
        const response = await axios.get(`http://${SERVER_IP}:8000/api/users`);
        if (
          response.data.error === false &&
          response.data.body &&
          Array.isArray(response.data.body)
        ) {
          const employeesData = response.data.body.filter(
            (user) => user.rol === "empleado_mantenimiento"
          );
          setEmployees(
            employeesData.map((user) => ({
              label: `${user.nombrePila} ${user.apPat} ${user.apMat}`,
              value: user.id.toString(),
            }))
          );
        } else {
          console.error("La respuesta de la API no es válida:", response.data);
        }
      } catch (error) {
        console.error("Error al obtener los empleados:", error);
      }
    };

    fetchUserInfo();
    fetchEmployees();
  }, []);

  const currentDate = moment().format("YYYY-MM-DD");
  const currentTime = moment().format("hh:mm");
  useEffect(() => {
    setDate(currentDate);
    setTime(currentTime);
  }, []);

  const sendDataToAPI = async () => {
    try {
      const response = await axios.post(
        `http://${SERVER_IP}:8000/api/Solicitud`,
        {
          prioridad: priority,
          departamento: departamentos,
          descripcion: description,
          fechaHora: date + " " + time,
          usuario_id_solicita: supervisorId,
          usuario_id_resuelve: null,
          estado_id: 1,
          activo: 1,
          tipo_mantenimiento: mantenimiento,
          maquina_id: machineNumber,
        }
      );
      return response;
    } catch (error) {
      console.error("Error al enviar datos:", error);
      throw error;
    }
  };

  const mantenimientoData = async () => {
    try {
      const response2 = axios.post(
        `http://${SERVER_IP}:8000/api/Mantenimiento`,
        {
          descripcion: descripcion,
          activo: 1,
        }
      );
      return response2;
    } catch (error) {
      console.error("Error al enviar los datos:", error);
      throw error;
    }
  };

  const clearForm = () => {
    setPlace("");
    setMachineNumber("");
    setDate(moment().format("YYYY-MM-DD"));
    setTime(moment().format("hh:mm"));
    setDescription("");
    setPriority("");
    setAssignee("");
  };

  const handleConfirm = () => {
    const machineError = machineNumberValidator(machineNumber);
    const departmentError = departmentValidator(departamentos);
    const priorityError = priorityValidator(priority);
    const maintenanceError = maintenanceValidator(mantenimiento);
    const descriptionError = descriptionValidator(description);

    if (
      machineError ||
      departmentError ||
      priorityError ||
      maintenanceError ||
      descriptionError
    ) {
      Alert.alert(
        "Error",
        machineError ||
          departmentError ||
          priorityError ||
          maintenanceError ||
          descriptionError
      );
      return;
    }

    Alert.alert(
      "Confirmar",
      "¿Estás seguro de enviar esta solicitud?",
      [
        {
          text: "Cancelar",
          style: "cancel",
        },
        {
          text: "Confirmar",
          onPress: async () => {
            try {
              const response = await sendDataToAPI();
  
              if (priority === "Alta") {
                await sendMessageViaWhatsApp(taskName, departamentos, machineNumber, description);
              }
              console.log("Enviado correctamente");
              clearForm();
            } catch (error) {
              console.error("Error al enviar datos:", error);
            }
          },
        },
      ],
      { cancelable: false }
    );
  };

  const placeholderTextColor = Platform.OS === "ios" ? "#999" : "#555";

  return (
    <Background>
      <View style={styles.headerContainer}>
        <Header style={styles.header}>Crear Solicitud</Header>
      </View>
      <ScrollView style={styles.container}>
        <View style={styles.card}>
          <View style={styles.inputRow}>
            <RNPickerSelect
              onValueChange={(value) => setDepartamentos(value)}
              items={DepartmentOptions}
              placeholder={{
                label: "Área",
                value: null,
              }}
              style={{
                inputIOS: {
                  ...styles.inputPicker,
                  width: 120,
                },
                inputAndroid: {
                  ...styles.inputPicker,
                  width: 165,
                },
                placeholder: {
                  color: "#999",
                  fontFamily: "Gilroy-Regular",
                  fontSize: 16,
                },
              }}
            />
            <RNPickerSelect
              onValueChange={(value) => setPriority(value)}
              items={priorityOptions}
              placeholder={{
                label: "Prioridad",
                value: null,
              }}
              style={{
                inputIOS: {
                  ...styles.inputPicker,
                  width: 130,
                },
                inputAndroid: {
                  ...styles.inputPicker,
                  width: 145,
                },
                placeholder: {
                  color: "#999",
                  fontFamily: "Gilroy-Regular",
                  fontSize: 16,
                  padding: 0,
                },
              }}
            />
          </View>
          <View style={styles.inputRow}>
            <RNPickerSelect
              onValueChange={(value) => setMantenimiento(value)}
              items={MantOptions}
              placeholder={{
                label: "Tipo de mantenimiento",
                value: null,
              }}
              style={{
                inputIOS: {
                  ...styles.inputPicker,
                  width: 120,
                },
                inputAndroid: {
                  ...styles.inputPicker,
                  width: 165,
                },
                placeholder: {
                  color: "#999",
                  fontFamily: "Gilroy-Regular",
                  fontSize: 16,
                },
              }}
            />
            <RNPickerSelect
              onValueChange={(value) => setMachineNumber(value)}
              items={MachineOptions}
              placeholder={{
                label: "Máquina",
                value: null,
              }}
              style={{
                inputIOS: {
                  ...styles.inputPicker,
                  width: 130,
                },
                inputAndroid: {
                  ...styles.inputPicker,
                  width: 145,
                },
                placeholder: {
                  color: "#999",
                  fontFamily: "Gilroy-Regular",
                  fontSize: 16,
                  padding: 0,
                },
              }}
            />
          </View>
          <View style={styles.inputRow}>
            <TextInput
              style={[styles.input, styles.inputFecha]}
              placeholder="Fecha"
              placeholderTextColor={placeholderTextColor}
              value={date}
              onChangeText={setDate}
              editable={false}
              textAlign="center"
            />
            <View style={{ width: 50 }} />
            <TextInput
              style={[styles.input, styles.inputHora]}
              placeholder="Hora"
              placeholderTextColor={placeholderTextColor}
              value={time}
              onChangeText={setTime}
              editable={false}
              textAlign="center"
            />
          </View>
          <TextInput
            style={[styles.input, styles.inputDescripcion]}
            placeholder="Descripción del problema"
            placeholderTextColor={placeholderTextColor}
            value={description}
            onChangeText={setDescription}
            multiline
            textAlignVertical="top"
          />
          <TouchableOpacity style={styles.button} onPress={handleConfirm}>
            <Text style={styles.buttonText}>Confirmar</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginBottom: 0,
  },
  card: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    marginBottom: 16,
    marginTop: 30,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 4,
    elevation: 1,
  },
  inputRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  inputLugar: {
    flex: 3,
    marginRight: 8,
  },
  inputPrioridad: {
    flex: 2,
  },
  inputFecha: {
    flex: 0.52,
    marginTop: 10,
  },
  inputHora: {
    flex: 0.57,
    marginTop: 10,
  },
  input: {
    fontFamily: "Gilroy-Regular",
    fontSize: 16,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: "#ddd",
    padding: 10,
    borderRadius: 6,
  },
  inputMaquina: {
    fontFamily: "Gilroy-Regular",
    fontSize: 16,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: "#ddd",
    padding: 10,
    borderRadius: 6,
    marginTop: 10,
    width: 130,
  },
  inputDescripcion: {
    fontFamily: "Gilroy-Regular",
    fontSize: 16,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: "#ddd",
    padding: 10,
    borderRadius: 6,
    height: 100,
    textAlignVertical: "top",
  },
  inputPicker: {
    fontFamily: "Gilroy-Regular",
    fontSize: 16,
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: "#ddd",
    borderRadius: 6,
    color: "#133036",
    marginTop: 10,
  },
  button: {
    backgroundColor: "#3DC253",
    borderRadius: 30,
    paddingVertical: 15,
    paddingHorizontal: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
  },
  buttonText: {
    color: "white",
    fontSize: 18,
    fontFamily: "Gilroy-Bold",
  },
  headerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    height: 120,
    paddingTop: 40,
    paddingHorizontal: 20,
    backgroundColor: "#133036",
  },
  header: {
    fontSize: 25,
    fontFamily: "Gilroy-Bold",
    color: "white",
  },
});

export default CreateSolicitud;
