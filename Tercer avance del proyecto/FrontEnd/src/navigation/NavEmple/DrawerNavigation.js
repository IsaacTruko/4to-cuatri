import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { COLORS } from "../../core/theme";
import { Ionicons } from "@expo/vector-icons";
import BottomTabNavigation from "./BottomTabNavigation";
import {
  MantenimientosScreen,
  HerramientasScreen,
  SolicitudesScreen,
  ProfileScreen,
} from "../../screens";

const Drawer = createDrawerNavigator();

const DrawerNavigation = () => {
  return (
    <Drawer.Navigator
      screenOptions={{
        drawerStyle: {
          backgroundColor: COLORS.white,
          width: 250,
        },
        headerStyle: {
          backgroundColor: COLORS.white,
        },
        headerShown: false,
        headerTintColor: COLORS.black,
        drawerLabelStyle: {
          color: COLORS.black,
          fontSize: 14,
          marginLeft: -10,
        },
      }}
    >
      <Drawer.Screen
        name="Home"
        options={{
          drawerLabel: "Home",
          title: "Home",
          headerShadowVisible: false,
          drawerIcon: () => (
            <Ionicons name="home-outline" size={24} color={COLORS.black} />
          ),
        }}
        component={BottomTabNavigation}
      />
      <Drawer.Screen
        name="SolicitudesScreen"
        options={{
          drawerLabel: "SolicitudesScreen",
          title: "SolicitudesScreen",
          headerShadowVisible: false,
          drawerIcon: () => (
            <Ionicons name="search-outline" size={24} color={COLORS.black} />
          ),
        }}
        component={SolicitudesScreen}
      />
      <Drawer.Screen
        name="CompletadasScreen"
        options={{
          drawerLabel: "CompletadasScreen",
          title: "CompletadasScreen",
          headerShadowVisible: false,
          drawerIcon: () => (
            <Ionicons name="search-outline" size={24} color={COLORS.black} />
          ),
        }}
        component={SolicitudesScreen}
      />
      <Drawer.Screen
        name="ProfileScreen"
        options={{
          drawerLabel: "ProfileScreen",
          title: "ProfileScreen",
          headerShadowVisible: false,
          drawerIcon: () => (
            <Ionicons name="heart-outline" size={24} color={COLORS.black} />
          ),
        }}
        component={ProfileScreen}
      />
    </Drawer.Navigator>
  );
};

export default DrawerNavigation;
