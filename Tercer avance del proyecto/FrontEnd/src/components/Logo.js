import React from 'react'
import { Image, StyleSheet } from 'react-native'

export default function Logo() {
  return <Image source={require('../assets/logo.png')} style={styles.image} />
}

const styles = StyleSheet.create({
  image: {
    width: 250,
    height: 250,
    marginBottom: 50,
    marginTop: 100,
    alignSelf: 'center',
    marginRight: 30,
  },
})
